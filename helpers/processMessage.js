const FACEBOOK_ACCESS_TOKEN = 'EAAcpQ4vM6GYBAMqfVK061or47eZAvT4jBlzPBpeoDU7rIrPS4IuBZAIUUEraLHLYe9qARyy8ZBpzpeXBCG7zGLLVH1lzU3gqzAyUSlti8wSJSG9OR3bgS9CzK2Agxcq5dVgXDk5a0wIcNQhWa8aIsigLBa5dKEoc6HEnJ1IdgZDZD';
const CAT_IMAGE_URL = 'https://botcube.co/public/blog/apiai-tutorial-bot/hosico_cat.jpg';

const request = require('request');

const API_AI_TOKEN = '6d654e8b74ca4666a6272a23498eb24f';
const apiAiClient = require('apiai')(API_AI_TOKEN);



const sendTextMessage = (senderId, text) => {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: FACEBOOK_ACCESS_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: senderId },
            message: { text },
        }
    });
};


const sendImage = (senderId, imageUri) => {
    return request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: FACEBOOK_ACCESS_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: senderId },
            message: {
                attachment: {
                    type: 'image',
                    payload: { url: CAT_IMAGE_URL }
                }
            }
        }
    });
};



module.exports = (event) => {
    const senderId = event.sender.id;
    const message = event.message.text;

    const apiaiSession = apiAiClient.textRequest(message, {sessionId: 'botcube_co'});

    apiaiSession.on('response', (response) => {

        console.log('api ai response', response);
        const result = response.result.fulfillment.speech;

        if (response.result.metadata.intentName === 'can i see some dolphins') {
            sendImage(senderId, result);
        } else {
            sendTextMessage(senderId, result);
        }
    });

    apiaiSession.on('error', error => console.log(error));
    apiaiSession.end();
};


